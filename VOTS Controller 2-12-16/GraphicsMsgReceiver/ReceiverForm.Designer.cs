﻿namespace GraphicsMsgReceiver
{
    partial class ReceiverForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.outBox = new System.Windows.Forms.TextBox();
            this.lastTimer = new System.Windows.Forms.Timer(this.components);
            this.commLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // outBox
            // 
            this.outBox.Location = new System.Drawing.Point(12, 14);
            this.outBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.outBox.Multiline = true;
            this.outBox.Name = "outBox";
            this.outBox.ReadOnly = true;
            this.outBox.Size = new System.Drawing.Size(360, 240);
            this.outBox.TabIndex = 0;
            // 
            // lastTimer
            // 
            this.lastTimer.Enabled = true;
            this.lastTimer.Interval = 25;
            this.lastTimer.Tick += new System.EventHandler(this.lastTimer_Tick);
            // 
            // commLabel
            // 
            this.commLabel.AutoSize = true;
            this.commLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commLabel.Location = new System.Drawing.Point(119, 285);
            this.commLabel.Name = "commLabel";
            this.commLabel.Size = new System.Drawing.Size(141, 25);
            this.commLabel.TabIndex = 1;
            this.commLabel.Text = "Comms Status";
            // 
            // ReceiverForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 336);
            this.Controls.Add(this.commLabel);
            this.Controls.Add(this.outBox);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ReceiverForm";
            this.Text = "Graphics Message Receiver";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReceiverForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox outBox;
        private System.Windows.Forms.Timer lastTimer;
        private System.Windows.Forms.Label commLabel;
    }
}

