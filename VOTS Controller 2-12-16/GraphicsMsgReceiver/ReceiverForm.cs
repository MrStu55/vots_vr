﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace GraphicsMsgReceiver
{
    public partial class ReceiverForm : Form
    {
        /// <summary>
        /// Socket that listens for multicast packets
        /// </summary>
        private Socket sock;
        /// <summary>
        /// Stopwatch used to time incoming messages
        /// </summary>
        private Stopwatch sw;
        /// <summary>
        /// Thread on which the socket listens
        /// </summary>
        private Thread receiveThread;

        public ReceiverForm()
        {
            InitializeComponent();

            sw = new Stopwatch();
            sw.Start();
            sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            try
            {
                IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("0.0.0.0"), 10001);
                sock.Bind(ipep);
                MulticastOption opt = new MulticastOption(IPAddress.Parse("224.0.0.1"), ipep.Address);
                sock.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, opt);
            }
            catch (Exception ex)
            {
                outBox.Text = "Error binding: " + ex.Message;
            }

            receiveThread = new Thread(() => ReadLoop());
            receiveThread.Start();
        }

        /// <summary>
        /// Read data in a loop
        /// </summary>
        private void ReadLoop()
        {
            while (true)
            { // Run until thread is aborted
                byte[] buffer = new byte[1500]; // Larger than necessary
                EndPoint remoteEp = new IPEndPoint(IPAddress.Any, 0);

                int readBytes = sock.ReceiveFrom(buffer, ref remoteEp);
                DisplayData(buffer);    // When data is read, deserialize and display
                sw.Restart(); // Reset timer so display counter is reset
            }
        }

        /// <summary>
        /// Display data received from the socket
        /// </summary>
        /// <param name="data"></param>
        private void DisplayData(byte[] data)
        {
            if (InvokeRequired)
            {
                MethodInvoker del = delegate { DisplayData(data); };
                this.Invoke(del);
            }
            else
            {
                GraphicsMsg msg;
                try
                {
                    MemoryStream ms = new MemoryStream(data);
                    XmlSerializer ser = new XmlSerializer(typeof(GraphicsMsg));
                    msg = (GraphicsMsg)ser.Deserialize(ms);
                }
                catch (Exception ex)
                {
                    outBox.Text = "Unable to deserialize message: " + ex.Message;
                    return;
                }

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("State: {0}{1}", msg.state, Environment.NewLine);
                sb.AppendFormat("Shuttle: {0}{1}", msg.shuttlePosition, Environment.NewLine);
                sb.AppendFormat("Scenario: {0}{1}", msg.scenarioId, Environment.NewLine);
                sb.AppendFormat("Aircraft: {0}{1}", msg.aircraftId, Environment.NewLine);
                outBox.Text = sb.ToString();
            }
        }

        /// <summary>
        /// When the timer elapses, update the display of how long since a message was received
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lastTimer_Tick(object sender, EventArgs e)
        {
            int sinceLast = (int)sw.ElapsedMilliseconds;
            if (sinceLast < 100)
            {   // Expected every 50 ms
                commLabel.BackColor = Color.Lime;
            }
            else
            {
                commLabel.BackColor = Color.Red;
            }
        }

        /// <summary>
        /// Shut down the thread while closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReceiverForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                sock.Close();
            }
            catch (Exception)
            {

            }
            try
            {
                receiveThread.Abort();
            }
            catch (Exception)
            {

            }
        }
    }
}
