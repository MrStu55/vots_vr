﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsMsgReceiver
{
    /// <summary>
    /// Message which is sent from Controller to Graphics as XML
    /// </summary>
    [Serializable]
    public class GraphicsMsg
    {
        /// <summary>
        /// Launch state, sent as integer
        /// </summary>
        public int state { get; set; }
        /// <summary>
        /// Position of shuttle for display
        /// </summary>
        public float shuttlePosition { get; set; }
        /// <summary>
        /// Identifier of launch scenario
        /// </summary>
        public int scenarioId { get; set; }
        /// <summary>
        /// Identifier of aircraft
        /// </summary>
        public int aircraftId { get; set; }
    }
}
