﻿using UnityEngine;
using System.Collections;

public class updateColour : MonoBehaviour {

    public Color myColour;


	// Use this for initialization
	void Start () {
        myColour = Color.white;
    }
	
	// Update is called once per frame
	void Update () {
        var materials = GetComponent<Renderer>().materials;
        materials[0].SetColor("_EmissionColor", myColour);

        // Another thing to note is that Unity 5 uses the concept of shader keywords extensively.
        // So if your material is initially configured to be without emission, then in order to enable emission, you need to enable // the keyword.
        materials[0].EnableKeyword("_EMISSION");
    }
}
