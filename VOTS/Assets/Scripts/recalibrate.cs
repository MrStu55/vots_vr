﻿using UnityEngine;
using System.Collections;
using Meta;

public class recalibrate : MonoBehaviour {

    public Transform target;
    public Transform refPoint;
    public Transform debugMarker;
    public GameObject metaHeadset;

    // Update is called once per frame
    void Update()
    {
        var heading = target.position - metaHeadset.transform.position;
        var distance = heading.magnitude;

        var newPos = refPoint.transform.position - heading;

        var dist = Vector3.Distance(target.position, metaHeadset.transform.position);
        var ori = Meta.IMULocalizer.Instance.imuOrientation;
        Debug.Log("Heading:" + heading + "Distance:" + distance + "  Orientation: [" + ori.x + "," + ori.y + "," +ori.z +"]");
        debugMarker.position = refPoint.transform.position - heading;

      //  metaHeadset.transform.position = debugMarker.position;
        if (Input.GetKey(KeyCode.R))
        {
            Debug.Log("Recalibrating....");

            metaHeadset.transform.position = debugMarker.position;

        }
        if (Input.GetKey(KeyCode.G))
        {
            Debug.Log("Recalibrating....");

            Meta.IMULocalizer.Instance.ResetLocalizer();

        }



    }
}
