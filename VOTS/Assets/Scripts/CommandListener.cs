﻿using UnityEngine;
using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using U3D.Threading.Tasks;

public class CommandListener : MonoBehaviour
{
    // read Thread
    Thread readThread;

    // udpclient object
    UdpClient client;

    // port number
    public int port = 10001;
    public string MulticastGroup = "224.0.0.1";

    // UDP packet store
    //public string lastReceivedPacket = "";

    public VOTS_State ScenarioState;
    public float ShuttlePosition;
    public int ScenarioID;
    public int aircraftID;
    public GameObject AnimationController;
    private AnimatorTrigger controller;

    void Awake ()
    {
        controller = AnimationController.GetComponent<AnimatorTrigger>();

    }

    // start from unity3d
    void Start()
    {
        // create thread for reading UDP messages
        readThread = new Thread(new ThreadStart(ReceiveData));
        readThread.IsBackground = true;
        readThread.Start();
        U3D.Threading.Dispatcher.Initialize();

    }

    // Unity Update Function
    void Update()
    {
        // check button "s" to abort the read-thread
        if (Input.GetKeyDown("q"))
            stopThread();
    }

    // Unity Application Quit Function
    void OnApplicationQuit()
    {
        stopThread();
    }

    // Stop reading UDP messages
    private void stopThread()
    {
        if (readThread.IsAlive)
        {
            readThread.Abort();
        }
        client.Close();
    }

    // receive thread function
    private void ReceiveData()
    {
        client = new UdpClient(port);
        client.Client.Blocking = false;

        var m_GrpAddr = IPAddress.Parse(MulticastGroup);
        client.JoinMulticastGroup(m_GrpAddr);

        // receive bytes
        IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);

        while (true)
        {
            try
            {
                byte[] data = client.Receive(ref anyIP);
                DisplayData(data);
            }
            catch (Exception err)
            {
                print(err.ToString());
            }

            Thread.Sleep(1);

        }
    }

    private void DisplayData(byte[] data)
    {

            GraphicsMsg msg;
            try
            {
                MemoryStream ms = new MemoryStream(data);
                XmlSerializer ser = new XmlSerializer(typeof(GraphicsMsg));
                msg = (GraphicsMsg)ser.Deserialize(ms);
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError("Unable to deserialize message: " + ex.Message);
                return;
            }
        
        ScenarioState = (VOTS_State)msg.state;
        ShuttlePosition = msg.shuttlePosition;
        ScenarioID = msg.scenarioId;
        aircraftID = msg.aircraftId;

        string cmd = "";
        cmd = Enum.GetName(typeof(VOTS_State), msg.state);

        Task.Run(() => {
            // log.text += "Testing\n"; => this would cause an exception, we are not in the main thread
            Task.RunInMainThread(() => controller.OnCommand(cmd));
        }); 
    }



    void OnDisable()
    {
        if (readThread != null)
            readThread.Abort();
        client.Close();
    }

}


/// <summary>
/// Message which is sent from Controller to Graphics as XML
/// </summary>
[Serializable]
public class GraphicsMsg
{
    /// <summary>
    /// Launch state, sent as integer
    /// </summary>
    public int state { get; set; }
    /// <summary>
    /// Position of shuttle for display
    /// </summary>
    public float shuttlePosition { get; set; }
    /// <summary>
    /// Identifier of launch scenario
    /// </summary>
    public int scenarioId { get; set; }
    /// <summary>
    /// Identifier of aircraft
    /// </summary>
    public int aircraftId { get; set; }
}

public enum VOTS_State
{
    INITIAL = 1,
    RETRACT = 2,
    ARM_GRAB = 3,
    INTRO_AIRCRAFT = 4,
    TAXI_COMPLETE = 5,
    LAUNCH_BAR = 6,
    HOOKUP = 7,
    SIGNAL_TENSION = 8,
    TENSION = 9,
    TENSION_COMPLETE = 10,
    MRT = 11,
    CRT = 12,
    COMMAND_LAUNCH = 13,
    LAUNCH = 14
}