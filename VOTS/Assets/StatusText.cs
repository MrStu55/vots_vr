﻿using UnityEngine;
using System.Collections;

public class StatusText : MonoBehaviour {
    private TextMesh txt;
    public string Text;


    void Awake()
    {
        txt = GetComponent<TextMesh>();
    }

	
	// Update is called once per frame
	void Update () {
        txt.text = Text;
    }
}
