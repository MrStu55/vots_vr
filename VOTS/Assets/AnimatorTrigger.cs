﻿using UnityEngine;
using System.Collections;

public class AnimatorTrigger : MonoBehaviour {

    private Animator animator;
    private TextMesh txt;

	// Use this for initialization
	void Awake () {
        animator = GetComponent<Animator>();
        txt = GetComponentInChildren<TextMesh>();
	}
	

    public void OnCommand(string command)
    {
        switch (command)
        {
            case "INITIAL":
                txt.text = "INITIAL";
                break;

            case "RETRACT":
                animator.SetTrigger("Trig_Retract");
                txt.text = "RETRACT";
                break;

            case "ARM_GRAB":
                animator.SetTrigger("Trig_Armgrab");
                txt.text = "ARM_GRAB";
                break;

            case "INTRO_AIRCRAFT":
                animator.SetTrigger("Trig_IntroAircraft");
                txt.text = "INTRO_AIRCRAFT";
                break;

            case "TAXI_COMPLETE":
             //   animator.SetTrigger("Trig_Armgrab");
                txt.text = "TAXI_COMPLETE";
                break;

            case "LAUNCH_BAR":
               // animator.SetTrigger("Trig_Armgrab");
                txt.text = "LAUNCH_BAR";
                break;

            case "HOOKUP":
              //  animator.SetTrigger("Trig_Armgrab");
                txt.text = "HOOKUP";
                break;

            case "SIGNAL_TENSION":
              //  animator.SetTrigger("Trig_Armgrab");
                txt.text = "SIGNAL_TENSION";
                break;

            case "TENSION":
             //   animator.SetTrigger("Trig_Armgrab");
                txt.text = "TENSION";
                break;

            case "MRT":
              //  animator.SetTrigger("Trig_Armgrab");
                txt.text = "MRT";
                break;

            case "CRT":
              //  animator.SetTrigger("Trig_Armgrab");
                txt.text = "CRT";
                break;

            case "COMMAND_LAUNCH":
             //   animator.SetTrigger("Trig_Armgrab");
                txt.text = "COMMAND_LAUNCH";
                break;

            case "LAUNCH":
               // animator.SetTrigger("Trig_Armgrab");
                txt.text = "LAUNCH";
                break;
        }
    }

}
