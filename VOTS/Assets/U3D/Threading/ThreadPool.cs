using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using U3D.Threading.Tasks;
using UnityEngine;
using System.Threading;

namespace U3D.Threading
{
	/// <summary>
	/// Provides a pool of threads that can be used to execute tasks, post work items, process 
	/// asynchronous I/O, wait on behalf of other threads, and process timers.
	/// </summary>
	public class ThreadPool
	{
		/// <summary>
		/// The number of requests to the thread pool that can be active concurrently. 
		/// All requests above that number remain queued until thread pool threads become available.
		/// </summary>
		public int maxThreads = 1;
		/// <summary>
		/// The difference between the maximum number of thread pool threads returned by the 
		/// GetMaxThreads method, and the number currently active.
		/// </summary>
		public int availableThreads
		{
			get { return maxThreads - m_currentThreads.Count; }
		}

		/// <summary>
		/// Describes a pooled thread
		/// </summary>
		public class PooledThread
		{
			internal Thread thread;
			internal bool aborted = false;
			internal Action action;
			/// <summary>
			/// Gets a value indicating whether this <see cref="U3D.Threading.ThreadPool+PooledThread"/> is enqueued.
			/// </summary>
			/// <value><c>true</c> if is enqueued; otherwise, <c>false</c>.</value>
			public bool isEnqueued
			{
				get { return thread == null; }
			}
			/// <summary>
			/// Aborts the pooled thread
			/// (The thread may not be executing, and it won't receive the ThreadAborted exception)
			/// </summary>
			public void Abort()
			{
				if (thread != null) 
				{
					thread.Abort ();
				} 
				else 
				{
					aborted = true;
				}	
			}
		}

		object m_sync = new object ();
		List<Thread> m_currentThreads = new List<Thread>();
		Queue<PooledThread> m_queuedActions = new Queue<PooledThread> ();
		/// <summary>
		/// Queues a method for execution. The method executes when a thread pool thread becomes available.
		/// </summary>
		/// <returns>PooledThread object describing the thread and its status.</returns>
		/// <param name="a">The action to be executed in the thread.</param>
		public PooledThread QueueAction(Action a)
		{
			PooledThread pt = new PooledThread () { action = a };
			lock (m_sync) 
			{
				if (availableThreads > 0) 
				{
					Thread th = new Thread(new ParameterizedThreadStart((_th) =>
						{
							try
							{								
								a();
							}
							finally
							{
								ThreadWillFinish((Thread)_th);
							}
						}));
					pt.thread = th;
					m_currentThreads.Add (th);
					th.Start(th);
				} 
				else 
				{
					m_queuedActions.Enqueue (pt);
				}
			}
			return pt;
		}

		void ThreadWillFinish(Thread th)
		{
			Action a = null;
			lock (m_sync) 
			{
				if (m_queuedActions.Count > 0) 
				{
					PooledThread pt = m_queuedActions.Dequeue ();
					if (pt.aborted) 
					{
						ThreadWillFinish (th);
					} 
					else 
					{
						pt.thread = th;
						a = pt.action;
					}
				}
				else 
				{
					m_currentThreads.Remove (th);
				}
			}
			if (a != null) 
			{
				a ();
				ThreadWillFinish (th);
			} 
		}
	}
}